# GraphQL awesome


## Some simple queries and mutations (GraphiQL)

The link for Postman collection for testing Rest API: https://www.getpostman.com/collections/84451de588dccfda452b

```json
// Add product

mutation {
  addProduct(productInput: { name: "Asdf"}) {
    _id
    name
  }
}


// Get all products 

query {
  products {
    name
  }
}


// Add transaction

mutation {
  addTransaction(transactionInput: {quantity: 123, productId: "5cfebf326fb13b357f410ee3"}) {
    quantity
  }
}

// Get products and transations

query {
  products {
    _id
    name
    transactions {
      _id
      quantity
      time
    }
  }
}


// Update transaction

mutation {
  updateTransaction (id: "5cfec54fe9e6003832d64695" transactionUpdate: {quantity: 111}) {
    quantity
  }
}


// Delete transaction

mutation {
	deleteTransaction (transactionId: "5cfec4dfe9e6003832d64693") {
		name
    transactions {
      _id
      time
      quantity
    }
  }
}
```