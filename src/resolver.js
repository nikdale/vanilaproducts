const resolvers = {
    Query: {
        product(root, { id }, { prisma }) {
            return prisma.product({ id });
        },
        products(root, args, { prisma }) {
            return prisma.query.products({}, info);
        },
        transaction(root, { id }, { prisma }) {
            return prisma.transaction({ id: id });
        },
        transactions(root, args, { prisma }) {
            return prisma.transactions();
        }
    },
    Mutation: {
        addProduct(root, { name }, { prisma }) {
            return prisma.createProduct({ name })
        },
        deleteProduct(root, { id }, { prisma }) {
            return prisma.deleteProduct({ id });
        },
        addTransaction(root, { quantity, productOwner }, { prisma }) {
            return prisma.createTransaction({
                quantity,
                productOwner: { connect: { id: productOwner } }
            })
        },
        updateTransaction(root, { quantity, id }, { prisma }) {
            return prisma.updateTransaction({
                where: { id: id },
                data: { quantity: quantity }
            });
        },
        deleteTransaction(root, { id }, { prisma }) {
            return prisma.deleteTransaction({ id });
        },
    },
    Subscription: {
        subscribeProducts: {
            subscribe: (root, args, { prisma }, info) => 
                prisma.$subscribe.product({
                    where: { mutation_in: ['CREATED', 'UPDATED', 'DELETED'] }
                }, info),

            resolve: async (payload, _, { prisma }) => {
                console.log('payload = ', payload)
                return payload
            },
        },
        subscribeTransactions: {
            subscribe: (root, args, { prisma }, info) => 
                prisma.$subscribe.transaction({
                    where: { mutation_in: ['CREATED', 'UPDATED', 'DELETED'] }
                }, info),

            resolve: async (payload, _, { prisma }) => {
                console.log('payload = ', payload)
                return payload
            },
        },
    },

    Product: {
        transactions(root, args, { prisma }) {
            return prisma.product({ id: root.id }).transactions();
        },
        id: (root, args, { prisma }) => root.id
    },
    Transaction: {
        productOwner(root, args, { prisma }) {
            return prisma.transaction({ id: root.id }).productOwner();
        },
        id: (root, args, { prisma }) => root.id
    }
}

module.exports = resolvers