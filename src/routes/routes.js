const express = require('express');
const { prisma } = require('../generated/prisma-client');
const router = express.Router();

router.get('/products/:id?', async (req, res) => {
    const id = req.query.id || req.body.id;
    let data;
    if (id) {
        data = await prisma.product({id});
    } else {
        data = await prisma.products()
    }
    res.json({data});
});

router.post('/products', async (req, res) => {
    const { name } = req.body;
    const productInput = {
        name: name
    }
    try {
        if (!name) {
            throw new Error("What about name?")
        }
        data = await prisma.createProduct({productInput});
        res.json(data)
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
});

router.patch('/products/update/:id?', async (req, res) => {
    const id = req.query.id || req.body.id;
    const { name } = req.body;
    const updateProduct = {
        name: name
    }
    try {
        data = await prisma.updateProduct({id, updateProduct});
        res.json({ data })
    } catch (error) {
        return res.status(404).json({ error: error.message });
    }
});

router.delete('/products/delete/:id?', async (req, res) => {
    const id = req.query.id || req.body.id;
    try {
        data = await prisma.deleteProduct({id});
        res.json({ data })
    } catch (error) {
        return res.status(404).json({ error: error.message });
    }
});

// ---

router.get('/transactions/:id?', async (req, res) => {
    const transactionId = req.query.id || req.body.id;
    try {
        const id = req.query.id || req.body.id;
        let data;
        if (id) {
            data = await prisma.transaction({id});
        } else {
            data = await prisma.transactions();
        }
        res.json({data});
    } catch (error) {
        return res.status(404).json({ error: error.message });
    }
});

router.post('/transactions', async (req, res) => {
    const { quantity, productId } = req.body;
    const transactionInput = {
        quantity: quantity,
        productId: productId,
    }
    try {
        if (!quantity || !productId) {
            throw new Error("Give us the data.");
        }

        data = await prisma.createTransaction({transactionInput});
        res.json(data)
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
});

router.patch('/transactions/update/:id?', async (req, res) => {
    const id = req.query.id || req.body.id;
    const {quantity, productId} = req.body;
    const transactionUpdate = {
        quantity: quantity,
        productId: productId,
    }
    try {
        if (!quantity || !productId) {
            throw new Error('What? Give me the data bro');
        } else if (!id) {
            throw new Error('And? ID? What to patch');
        }
        data = await prisma.updateTransaction({id, transactionUpdate});
        res.json({ data })
    } catch (error) {
        return res.status(404).json({ error: error.message });
    }
});

router.delete('/transactions/delete/:id?', async (req, res) => {
    const transactionId = req.query.id || req.body.id;
    try {
        if (!transactionId) {
            throw new Error("Give us the target! (id)");
        }
        data = await prisma.deleteTransaction({transactionId});
        res.json({ data })
      } catch (error) {
        return res.status(404).json({ error: error.message });
      }
});

module.exports = router;